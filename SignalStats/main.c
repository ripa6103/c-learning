/*
 * main.c
 *
 *  Created on: 10 Aug 2023
 *      Author: rickpandey
 */


#include<stdio.h>
#include<stdlib.h>
#include <math.h>

#define SIG_LENGTH 300

extern double random_signal[SIG_LENGTH];
double calc_signal_mean(double *, int);
double calc_signal_variance(double *, double, int);
double calc_signal_std(double);

int main(){
	double signal_mean = calc_signal_mean(&random_signal[0], SIG_LENGTH);
	double variance = calc_signal_variance(&random_signal[0], signal_mean, SIG_LENGTH);
	double standard_deviation = calc_signal_std(variance);
	printf("\n\nSignal Mean = %f\n\n", signal_mean);
	printf("\n\nSignal Variance = %f\n\n", variance);
	printf("\n\nSignal Standard deviation = %f\n\n", standard_deviation);

}


double calc_signal_mean(double *sig_src_arr, int signal_length){
	double signal_mean = 0.0;
	for (int i=0; i<signal_length;i++){
		signal_mean += sig_src_arr[i];
	}
	signal_mean /= (double)signal_length;
	return signal_mean;
}

double calc_signal_variance(double *sig_src_arr, double signal_mean, int signal_length){
	double variance = 0.0;
	for (int i=0; i<signal_length;i++){
		variance += pow(sig_src_arr[i] - signal_mean, 2);
	}
	variance /= (double)signal_length;
	return variance;
}

double calc_signal_std(double variance){
	double standard_deviation;
	standard_deviation = pow(variance, 0.5);
	return standard_deviation;
}
